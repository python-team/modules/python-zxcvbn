python-zxcvbn (4.4.28-3.1) UNRELEASED; urgency=medium

  [ Carl Suster ]
  * d/watch: Track tags not the (empty) releases page on GitHub.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

 -- Carl Suster <carl@contraflo.ws>  Sat, 23 Jul 2022 14:21:37 +1000

python-zxcvbn (4.4.28-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 16:07:47 -0400

python-zxcvbn (4.4.28-2) unstable; urgency=medium

  * Team upload.
  * Drop Python 2 support (Closes: #938287).

 -- Ondřej Nový <onovy@debian.org>  Fri, 13 Sep 2019 10:26:09 +0200

python-zxcvbn (4.4.28-1) unstable; urgency=medium

  [ Sabino Par ]
  * New upstream release.
  * Update copyright.

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0.

 -- Sabino Par <sprab@onenetbeyond.org>  Tue, 03 Sep 2019 23:00:00 +0100

python-zxcvbn (4.4.27-1) unstable; urgency=medium

  [ Sabino Par ]
  * New upstream release
  * d/control: Update std-version to 4.2.0
  * d/watch: move from pypi to github.

  [ Mattia Rizzolo ]
  * d/watch: Fix archive matching
  * d/rules: Do not install the zxcvbn binary in the python2 package.
    Closes: #896489
  * d/control:
    + Bump Standards-Version to 4.3.0, no changes needed.
    + Use the new debhelper-compat notation and drop d/compat.
    + Bump the debhelper compat level to 12.
    + Set Rules-Requires-Root:no.
    + Set Testsuite:autopkgtest-pkg-python.
    + Tweak the descriptions to appease lintian.
  * Install the README.rst file in all packages.

  [ Chris Lamb ]
  * Remove trailing whitespaces.

  [ Ondřej Nový ]

  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

 -- Sabino Par <sprab@onenetbeyond.org>  Wed, 13 Feb 2019 00:49:46 +0100

python-zxcvbn (4.4.25-1) unstable; urgency=medium

  [ Sabino Par ]
  * New upstream release
  * Update std-version to 4.1.4

 -- Sabino Par <sprab@onenetbeyond.org>  Sat, 14 Apr 2018 17:36:50 +0200

python-zxcvbn (4.4.19-2) UNRELEASED; urgency=medium

  * d/changelog: Remove trailing whitespaces

 -- Ondřej Nový <onovy@debian.org>  Mon, 12 Mar 2018 23:10:41 +0100

python-zxcvbn (4.4.19-1) unstable; urgency=medium

  [ Sabino Par ]
  * New upstream release

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Gianfranco Costamagna ]
  * Update std-version to 4.1.3, no changes required.

 -- Sabino Par <sprab@onenetbeyond.org>  Fri, 16 Feb 2018 12:11:20 +0200

python-zxcvbn (4.4.16-1) unstable; urgency=medium

  * New upstream release
  * Update std-version to 4.1.0

 -- Sabino Par <sprab@onenetbeyond.org>  Sun, 10 Sep 2017 15:11:20 +0200

python-zxcvbn (4.4.15-1) unstable; urgency=medium

  * Upload to unstable

 -- Sabino Par <sprab@onenetbeyond.org>  Sat, 22 Jul 2017 15:11:20 +0200

python-zxcvbn (4.4.15-0.1) experimental; urgency=low

  * New upstream release
  * Update std-version to 4.0.0

 -- Sabino Par <sprab@onenetbeyond.org>  Fri, 21 Jul 2017 09:05:12 +0200

python-zxcvbn (4.4.14-0.1) experimental; urgency=low

  [Sabino Par]
  * New maintainer (Closes: #855638)
    - Thanks Riley for your work!
  * Fixed change upstream source to the active fork (Closes: #850910)
    - Update to new source code fork 4.4.14 release
    - update copyright file for new release and remove CC-BY-SA-3.0
      and public-domain references
  * Added new Python3 package port
  * Update to new compat level 10
  * Update watch file to point to new package location
  * Switch packaging to pybuild
  * Update standard version to 3.9.8, no changes required

  [Ondřej Nový]
  * Fixed VCS URL (https)

 -- Sabino Par <sprab@onenetbeyond.org>  Sat, 25 Mar 2017 15:30:28 +0100

python-zxcvbn (1.0+git20130503.bc1c2d-1) unstable; urgency=low

  * Initial release (Closes: #707786)

 -- Riley Baird <BM-2cVqnDuYbAU5do2DfJTrN7ZbAJ246S4XiX@bitmessage.ch>  Thu, 12 Mar 2015 06:43:07 +1100
